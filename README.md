# Kludge KVM

6/22: Recently I got a new monitor, a Gigabyte M32U, which has a built in KVM. I retired this setup in favor of that, but it lacked a software switch. [Here are some notes on how I got that working](M32U.md).

---


☕ &nbsp; _Is this project helpful to you? [Please consider buying me a coffee](https://pay.feralresearch.org/tip)._

Small script that monitors for when keyboard is disconnected or connected and switches the display input (via ddcctl) appropriately. Set it to run every few seconds.

Originally I used [watch](<https://en.wikipedia.org/wiki/Watch_(command)>). Now I'm simply using a bash loop, this is the dumbest possible solution, but works :D

# Setup

Launchctl process `org.feralresearch.kvm-kludge` launches `launchAtLogin.sh` which loops and calls `keyboardDetect.sh` every 3 seconds.

## Notes:

- Would be cool if I could figure out a disconnect event and then wouldn't need to poll at all

## Links

https://ravikiranj.net/posts/2019/code/how-change-monitor-input-source-command-line/  
https://github.com/kfix/ddcctl  
https://github.com/snosrap/xpc_set_event_stream_handler  
https://github.com/himbeles/mac-device-connect-daemon
